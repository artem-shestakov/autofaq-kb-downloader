package main

import (
	"flag"
	"fmt"
	"time"

	"gitlab.com/artem-shestakov/autofaq-kb-downloader/internal/app/model"
)

// Flag's vars
var host, port, dir string
var userTokens model.Tokens

// Init flags vars
func init() {
	flag.StringVar(&host, "host", "127.0.0.1", "AutoFAQ IP address")
	flag.StringVar(&port, "port", "80", "AutoFAQ port")
	flag.StringVar(&dir, "dir", "./", "directory for downloading files")
	flag.Var(&userTokens, "tokens", "comma-separeted AutoFAQ user's CRUD tokens")
	flag.Parse()
}

func main() {
	if len(userTokens) == 0 {
		fmt.Printf("%s [%s] -- %s\n", time.Now().Format("2006/01/02 03:04"), "ERROR", "Please specify 'tokens' flag")
		return
	}
	dl := model.New(host, port, dir)
	dl.Run(userTokens)
	dl.Wg.Wait()
	dl.InfoLog("Downloader stoped")
}
