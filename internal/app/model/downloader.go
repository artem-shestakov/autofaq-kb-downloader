package model

import (
	"encoding/json"
	"fmt"
	"io/ioutil"
	"log"
	"net/http"
	"os"
	"strings"
	"sync"
	"time"
)

// Downloader structure
type Downloader struct {
	Wg   sync.WaitGroup
	host string
	port string
	dir  string
}

// New Downloader
func New(host string, port string, dir string) *Downloader {
	return &Downloader{
		Wg:   sync.WaitGroup{},
		host: host,
		port: port,
		dir:  dir,
	}
}

// Run start downloading
func (d *Downloader) Run(tokens Tokens) {
	d.InfoLog("Downloader starting...")
	var perm os.FileMode = 0755
	if _, err := os.Stat(d.dir); err != nil {
		if err := os.MkdirAll(d.dir, perm); err != nil {
			fmt.Printf("%s [%s] -- %s %s\n", time.Now().Format("2006/01/02 03:04"), "ERROR", "Can't create directory for downloading files", err.Error())
			d.ErrorLog("Can't create directory for downloading files")
			return
		}

	}
	for _, token := range tokens {
		d.Wg.Add(1)
		go d.getServices(token)
	}
}

// getService list all token's services
func (d *Downloader) getServices(token string) {
	var services Services
	defer d.Wg.Done()
	client := &http.Client{}
	req, err := http.NewRequest("GET", fmt.Sprintf("http://%s:%s/core-api/crud/api/v1/services", d.host, d.port), nil)
	if err != nil {
		fmt.Println(err.Error())
		return
	}
	req.Header.Add("AUTOFAQ-User-Token", token)
	resp, err := client.Do(req)
	if err != nil {
		d.ErrorLog(fmt.Sprintf("Getting services. Request to server error. %s", err.Error()))
		return
	}
	defer resp.Body.Close()

	body, err := ioutil.ReadAll(resp.Body)
	if err != nil {
		d.ErrorLog(fmt.Sprintf("Getting services. Can't read request body. %s", err.Error()))
		return
	}
	if err := json.Unmarshal(body, &services.Services); err != nil {
		d.ErrorLog(fmt.Sprintf("Unmarshal JSON error. %s", err.Error()))
		return
	}

	for _, service := range services.Services {
		d.Wg.Add(1)
		go d.download(service, token)
	}
}

// download xls file
func (d *Downloader) download(service Service, token string) {
	defer d.Wg.Done()
	client := &http.Client{}
	req, err := http.NewRequest("GET", fmt.Sprintf("http://%s:%s/core-api/crud/api/v1/services/xls?service_ids=%d", d.host, d.port, service.ID), nil)
	if err != nil {
		fmt.Println(err)
		return
	}
	req.Header.Add("AUTOFAQ-User-Token", token)

	for attempts := 1; attempts <= 3; attempts++ {
		resp, err := client.Do(req)
		if err != nil {
			d.ErrorLog(fmt.Sprintf("Download file. Request to server error. %s", err.Error()))
			return
		}
		if resp.StatusCode == 200 {
			defer resp.Body.Close()

			fmt.Println(service.Name, service.ID)
			body, err := ioutil.ReadAll(resp.Body)
			if err != nil {
				d.ErrorLog(fmt.Sprintf("Download file. Can't read request body. %s", err.Error()))
				return
			}

			d.correctName(&service)
			out, err := os.Create(fmt.Sprintf("%s%s_%s.xls", d.dir, service.Name, time.Now().Format("02_01_2006")))
			if err != nil {
				d.ErrorLog(fmt.Sprintf("Can't create file. %s", err.Error()))
			}
			out.Write(body)
			d.InfoLog(fmt.Sprintf("Downloaded: %s Attempt: %d ", service.Name, attempts))
			break
		}

	}

}

// correctName replace spaces and \ in service name
func (d *Downloader) correctName(service *Service) {
	service.Name = strings.Replace(service.Name, "/", "_", -1)
	service.Name = strings.Replace(service.Name, " ", "_", -1)
}

func (d *Downloader) ErrorLog(message string) {
	logFile, err := os.OpenFile("/var/log/kb-downloader.log", os.O_APPEND|os.O_CREATE|os.O_WRONLY, 0644)
	if err != nil {
		log.Println(err)
	}
	defer logFile.Close()
	logger := log.New(logFile, "ERROR", log.LstdFlags)
	logger.Println(message)
}

func (d *Downloader) InfoLog(message string) {
	logFile, err := os.OpenFile("/var/log/kb-downloader.log", os.O_APPEND|os.O_CREATE|os.O_WRONLY, 0644)
	if err != nil {
		log.Println(err)
	}
	defer logFile.Close()
	logger := log.New(logFile, "INFO", log.LstdFlags)
	logger.Println(message)
}
