package model

import (
	"fmt"
	"strings"
)

// Tokens list of user's tokens
// This object used with 'flag'
type Tokens []string

func (t *Tokens) String() string {
	return fmt.Sprint(*t)
}

func (t *Tokens) Set(value string) error {
	for _, token := range strings.Split(value, ",") {
		*t = append(*t, token)
	}
	return nil
}
