package model

// Service structure
type Service struct {
	ID     int    `json:"service_id"`
	Name   string `json:"name"`
	Status string `json:"status"`
}

// Services list of services
type Services struct {
	Services []Service `json:"services"`
}
