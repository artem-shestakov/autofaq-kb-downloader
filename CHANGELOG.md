# CHANGELOG

<!--- next entry here -->

## 0.1.1
2021-07-23

### Fixes

- **downloader:** Change permission of file's directory fron 744 -> 755 (fa9cd24a28973dfe4d462528c06d1861876d5ad9)

### Other changes

- Merge branch 'develop' into 'main' (0fe4b7c380c02025cd4c6baa0014feb6f6278df3)

## 0.1.0
2021-07-23

### Features

- **log:** Add loggin in /var/log/kb-downloader.log (8051b00518eeb067635683f271c5ac9caab2f85c)

### Other changes

- Merge branch 'develop' into 'main' (e44614126a4b492d0b960ba9c070a8ef740340a4)